﻿using System;
using System.Collections.Generic;
using System.Text;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetPostsByUserQuerry : IRequest<List<PostVM>>
    {
        public string UserId { get; set; }
    }
}
