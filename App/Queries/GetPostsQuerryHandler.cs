﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetPostsQuerryHandler : HandlerWIthDBConnection, IRequestHandler<GetPostsQuerry, List<PostVM>>
    {
        public GetPostsQuerryHandler(IDataBase context):base(context){ } 

        public async Task<List<PostVM>> Handle(GetPostsQuerry request, CancellationToken cancellationToken)
        {
            List<PostVM> result = new List<PostVM>();
            
            List<string> subs  = new List<string>();
            _context.Subscriptions.Where(x => x.TargetUserId == request.CurrentUserId).ToList().ForEach(y => subs.Add(y.TargetUserId));

            
            _context.Posts.Where(p => subs.Contains(p.CreatedBy)).ToList()
                .ForEach(p => 
                    result.Add(new PostVM {
                        Body = p.Description,
                        AuthorName = p.CreatedBy,
                        PictureByteArray = _context.UserPictures.Single(o => o.UserId == p.CreatedBy).PictureByteArray,
                        Likes = _context.Likes.Where(l => l.PostId ==p.Id).Count(), 
                        IsLikedByMe = _context.Likes.ToList().Exists(q => q.CreatedBy == request.CurrentUserId)
                    })
                );

            
            return result;
        }
    }
}
