﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetPostsQuerry : IRequest<List<PostVM>>
    {
        public string CurrentUserId { get; set; }
    }
}
