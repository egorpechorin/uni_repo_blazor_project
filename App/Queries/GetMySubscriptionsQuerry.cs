﻿using System;
using System.Collections.Generic;
using System.Text;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetMySubscriptionsQuerry : IRequest<List<UserVM>>
    {
        public string UserId { get; set; }
    }
}
