﻿using System;
using System.Collections.Generic;
using System.Text;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetCommentsQuerry : IRequest<List<CommentVM>>
    {
        public int PostId { get; set; }
    }
}
