﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class SearchUsersQuerryHandler : HandlerWIthDBConnection, IRequestHandler<SearchUsersQuerry, List<UserVM>>
    {
        private readonly IAspUsersManager _aspUserManager;

        public SearchUsersQuerryHandler(IDataBase context, IAspUsersManager aspUserManager) : base(context)
        {
            _aspUserManager = aspUserManager;
        }

        public async Task<List<UserVM>> Handle(SearchUsersQuerry request, CancellationToken cancellationToken)
        {
            List<UserVM> result = new List<UserVM>();

            try
            {
                _aspUserManager.GetUsersByPartOfName(request.FirstNameLetters).ToList()
                .ForEach(user => result.Add(new UserVM()
                {
                    Name = user.Name,
                    PictureByteArray = _context.UserPictures.ToList().Single(up => up.UserId == user.Id).PictureByteArray,
                }));
            }
            catch (Exception e)
            {
                // log exception in future
                return null;
            }
            
            return result.Any() ? result : null;
        }
    }
}
