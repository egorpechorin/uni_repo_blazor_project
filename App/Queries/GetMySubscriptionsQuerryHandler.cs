﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetMySubscriptionsQuerryHandler : HandlerWIthDBConnection, IRequestHandler<GetMySubscriptionsQuerry, List<UserVM>>
    {
        public GetMySubscriptionsQuerryHandler (IDataBase context):base(context){}

        public async Task<List<UserVM>> Handle(GetMySubscriptionsQuerry request, CancellationToken cancellationToken)
        {
            List<UserVM> result = new List<UserVM>();

            _context.Subscriptions.Where(x => x.TargetUserId == request.UserId).ToList()
                .ForEach(y => _context.UserPictures.Where(z => z.UserId == y.TargetUserId).ToList()
                .ForEach(q => result.Add(new UserVM { PictureByteArray = q.PictureByteArray, Name = q.UserId})));
            
            return result;
        }
    }
}
