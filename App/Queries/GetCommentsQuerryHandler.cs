﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetCommentsQuerryHandler : HandlerWIthDBConnection, IRequestHandler<GetCommentsQuerry, List<CommentVM>>
    {
        public GetCommentsQuerryHandler(IDataBase context) : base(context){}

        public async Task<List<CommentVM>> Handle(GetCommentsQuerry request, CancellationToken cancellationToken)
        {
            List<CommentVM> result = new List<CommentVM>();

            _context.Comments.Where(x => x.PostId == request.PostId).ToList().ForEach(c =>
                result.Add(new CommentVM { 
                    Body = c.Body, 
                    AuthorPhoto = _context.UserPictures.Find(c.CreatedBy).PictureByteArray,
                    Name = c.CreatedBy
                }));
           
            return result;
        }
    }
}
