﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using App.ViewModels;
using MediatR;

namespace App.Queries
{
    public class GetPostsByUserQuerryHandler : HandlerWIthDBConnection, IRequestHandler<GetPostsByUserQuerry, List<PostVM>>
    {
        public GetPostsByUserQuerryHandler(IDataBase context):base(context){}


        public async Task<List<PostVM>> Handle(GetPostsByUserQuerry request, CancellationToken cancellationToken)
        {
            List<PostVM> result = new List<PostVM>();

            _context.Posts.Where(x => x.CreatedBy == request.UserId).ToList()
                .ForEach(p =>
                    result.Add(new PostVM
                    {
                        Body = p.Description,
                        AuthorName = p.CreatedBy,
                        PictureByteArray = _context.UserPictures.Single(o => o.UserId == p.CreatedBy).PictureByteArray,
                        Likes = _context.Likes.Where(l => l.PostId == p.Id).Count(),
                        IsLikedByMe = _context.Likes.ToList().Exists(q => q.CreatedBy == request.UserId)
                    })
                );

            return result;
        }
    }
}
