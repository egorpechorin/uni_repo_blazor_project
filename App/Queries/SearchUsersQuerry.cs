﻿using App.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Queries
{
    public class SearchUsersQuerry : IRequest<List<UserVM>>
    {
        public string FirstNameLetters;
    }
}
