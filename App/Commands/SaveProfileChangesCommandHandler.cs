﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class SaveProfileChangesCommandHandler : HandlerWIthDBConnection, IRequestHandler<SaveProfileChangesCommand, MediatRCommandResult>
    {
        public SaveProfileChangesCommandHandler(IDataBase context) : base(context) { }

        public async Task<MediatRCommandResult> Handle(SaveProfileChangesCommand request, CancellationToken cancellationToken)
        {
            if (request.UserID == default) return MediatRCommandResult.FAIL;

            UserPicture uIm = _context.UserPictures.Single(i => i.UserId.Equals(request.UserID));

            if (uIm == null)
            {
                _context.UserPictures.Add(new UserPicture
                {
                    UserId = request.UserID,
                    PictureByteArray = request.NewPhoto,
                });
            }
            else
            {
                uIm.PictureByteArray = request.NewPhoto;
            }

            try
            {
                await _context.SaveChangesAsync(new CancellationToken());
            }
            catch (Exception e)
            {
                //log it in future!
                return MediatRCommandResult.FAIL;
            }
            return MediatRCommandResult.OK;
        }
    }
}
