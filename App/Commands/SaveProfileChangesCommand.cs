﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class SaveProfileChangesCommand : IRequest<MediatRCommandResult>
    {
        public string UserID { get; set; }
        public byte[] NewPhoto { get; set; }
    }
}
