﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class SavePostChangesCommandHandler : HandlerWIthDBConnection,  IRequestHandler<SavePostChangesCommand, MediatRCommandResult>
    {
        public SavePostChangesCommandHandler(IDataBase context):base(context){}

        public async Task<MediatRCommandResult> Handle(SavePostChangesCommand request, CancellationToken cancellationToken)
        {
            if(request.PostId == null)
            {
                if(request.UserId == null || request.UserId == "")  return MediatRCommandResult.FAIL;

                _context.Posts.Add(new Post {
                    CreatedBy = request.UserId,
                    CreatedDateTime = DateTime.UtcNow,
                    Description = request.Description,
                    ImageByteArray = request.ImageByteArray
                });
            }
            else
            {
                Post post = _context.Posts.Single(p => p.Id == request.PostId);

                if(post == null || request.Description == default && request.ImageByteArray == default) return MediatRCommandResult.FAIL;

                post.Description = request.Description;
                post.ImageByteArray = request.ImageByteArray; 
            }

            try
            {
                await _context.SaveChangesAsync(new CancellationToken());
            } catch (Exception e)
            {
                //log it in future!
                return MediatRCommandResult.FAIL;
            }
            return MediatRCommandResult.OK;
        }
    }
}
