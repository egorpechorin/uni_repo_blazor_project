﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class ChangeLikeStatusCommand : IRequest<MediatRCommandResult>
    {
        public string CurrentUserID { get; set; }
        public int PostId { get; set; }

        public string CombineID() { return PostId.ToString() + CurrentUserID; }
    }
}
