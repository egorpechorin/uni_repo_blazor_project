﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using App.Common;
using App.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class ChangeLikeStatusCommandHandler : HandlerWIthDBConnection,  IRequestHandler<ChangeLikeStatusCommand, MediatRCommandResult>
    {
        public ChangeLikeStatusCommandHandler(IDataBase context) : base(context) { }

        public async Task<MediatRCommandResult> Handle(ChangeLikeStatusCommand request, CancellationToken cancellationToken)
        {
            Like like = _context.Likes.Single(l => l.CombineID().Equals(request.CombineID()));

            if(like == null)
            {
                _context.Likes.Add(new Like { 
                    PostId = request.PostId, 
                    CreatedBy = request.CurrentUserID, 
                    CreatedDateTime = DateTime.UtcNow
                });
            }
            else
            {
                _context.Likes.Remove(like);
            }

            try
            {
                await _context.SaveChangesAsync(new CancellationToken());
            }
            catch (Exception e)
            {
                //log it in future!
                return MediatRCommandResult.FAIL;
            }
            return MediatRCommandResult.OK;
        }
    }
}
