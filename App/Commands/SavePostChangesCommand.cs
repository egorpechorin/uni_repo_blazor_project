﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace App.Commands
{
    public class SavePostChangesCommand : IRequest<MediatRCommandResult>
    { 
        public string UserId { get; set; }
        public int? PostId { get; set; }
        public byte[] ImageByteArray { get; set; }
        public string Description { get; set; }
    }
}
