﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.DataTransferObjects
{
    public class AspUserDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
    }
}
