﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App.Common.Interfaces
{
    public interface IDataBase
    {
        DbSet<Post> Posts { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }
        DbSet<Like> Likes { get; set; }
        DbSet<Comment> Comments { get; set; }
        DbSet<UserPicture> UserPictures { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

    }
}
