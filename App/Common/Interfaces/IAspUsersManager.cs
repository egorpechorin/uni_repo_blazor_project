﻿using App.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Interfaces
{
    public interface IAspUsersManager
    {
        AspUserDTO GetSpecificUserById(int id);
        AspUserDTO GetSpecificUserByName(string name);
        AspUserDTO GetSpecificUserByPhoneNumber(string phoneNumer);
        AspUserDTO GetSpecificUserByEmail(string email);

        IEnumerable<AspUserDTO> GetUsersByPartOfName(string namePart);
    }
}
