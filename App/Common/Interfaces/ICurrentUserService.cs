﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Interfaces
{
    public interface ICurrentUserService
    {
        string UserId { get; }
    }
}
