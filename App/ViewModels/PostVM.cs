﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.ViewModels
{
    public class PostVM
    {
        public byte[] PictureByteArray { get; set; }
        public string AuthorName { get; set; }
        public string Body { get; set; }
        public int Likes { get; set; }
        public bool IsLikedByMe { get; set; }
        public int Id { get; set; }
    }
}
