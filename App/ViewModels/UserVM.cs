﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.ViewModels
{
    public class UserVM
    {
        public string Name { get; set; }
        public byte[] PictureByteArray { get; set; }
    }
}
