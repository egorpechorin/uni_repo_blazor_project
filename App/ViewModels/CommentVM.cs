﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.ViewModels
{
    public class CommentVM
    {
        public byte[] AuthorPhoto { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }

    }
}
