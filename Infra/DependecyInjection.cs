﻿
using App.Common.Interfaces;
using Domain.Entities;
using Infra.Data;
using Infra.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra
{
    public static class DependecyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            //configure services

            services.AddDbContext<InsolyContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("InsolyContext"),
                    b => b.MigrationsAssembly(typeof(InsolyContext).Assembly.FullName)));

            services.AddDefaultIdentity<IdentityUser>(options =>
                options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<InsolyContext>();

            
            //set access to services from APPLICATION layer

            services.AddScoped<IDataBase>(provider => provider.GetService<InsolyContext>());
            services.AddScoped<IAspUsersManager, AspUsersManager>();

            services.AddScoped<ICurrentUserService, CurrentUserService>();

            return services;
        }
    }
}
