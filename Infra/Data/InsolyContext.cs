﻿using App.Common.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using IdentityServer4.EntityFramework.Options;
using IdentityServer4.EntityFramework.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Infra.Data

{

    public class InsolyContext : ApiAuthorizationDbContext<IdentityUser>,
        //IdentityDbContext<User>,
        IDataBase

    {
       
        public InsolyContext(
           DbContextOptions<InsolyContext> options
           , IOptions<OperationalStoreOptions> operationalStoreOptions
           )
           : base(options , operationalStoreOptions
                 )
        {
           
        }


        public DbSet<Post> Posts { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserPicture> UserPictures { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            //some logic in future for event sourcing (as example)
            return base.SaveChangesAsync(cancellationToken);
        }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Post>().ToTable("Post");


            modelBuilder.Entity<Comment>().ToTable("Comment");
            modelBuilder.Entity<Comment>()
                .HasKey(c => new { c.PostId, c.CreatedBy });
            

            modelBuilder.Entity<Like>().ToTable("Like");
            modelBuilder.Entity<Like>()
                .HasKey(c => new { c.PostId, c.CreatedBy });


            modelBuilder.Entity<Subscription>().ToTable("Subscription");
            modelBuilder.Entity<Subscription>()
                .HasKey(c => new {
            //c.Id,
            c.CreatedBy, c.TargetUserId});


            modelBuilder.Entity<UserPicture>().ToTable("UserPicture");
            modelBuilder.Entity<UserPicture>()
                .HasKey(c => new { c.UserId});

            base.OnModelCreating(modelBuilder);
            
        }
    
    
    }

}
