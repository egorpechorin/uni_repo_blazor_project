﻿using App.Common.Interfaces;
using App.DataTransferObjects;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Services
{
    public class AspUsersManager : IAspUsersManager
    {
        private readonly UserManager<IdentityUser> _userManager;

        public AspUsersManager(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public AspUserDTO GetSpecificUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public AspUserDTO GetSpecificUserById(int id)
        {
            throw new NotImplementedException();
        }

        public AspUserDTO GetSpecificUserByName(string name)
        {
            throw new NotImplementedException();
        }

        public AspUserDTO GetSpecificUserByPhoneNumber(string phoneNumer)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AspUserDTO> GetUsersByPartOfName(string namePart)
        {
            List<AspUserDTO> data = new List<AspUserDTO>();

            /*
            _userManager.Users.Where(user => user.NormalizedUserName.Contains(namePart.ToUpper())).ToList()
                .ForEach(user => data.Add(new AspUserDTO() { Id = user.Id, }));
            */
            return data.Any() ? data : null;
        }
    }
}
