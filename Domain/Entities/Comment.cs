﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Comment : AuditEntity
    {
       // public int Id { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public string Body { get; set; }
    }
}
