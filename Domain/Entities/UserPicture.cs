﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class UserPicture
    {
        public string UserId { get;set;}
        public byte[] PictureByteArray { get; set; }

    }
}
