﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Post : AuditEntity
    {
        public int Id { get; set; }
        public byte[] ImageByteArray { get; set; }
        public string Description { get; set; }
    }
}
