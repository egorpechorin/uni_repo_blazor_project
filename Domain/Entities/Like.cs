﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Like : AuditEntity
    {
        public int PostId { get; set; }
        public Post Post { get; set; } 

        public string CombineID() { return PostId.ToString()+CreatedBy; }
    }
}
