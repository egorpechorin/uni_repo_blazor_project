﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Subscription : AuditEntity
    {
        public string TargetUserId { get; set; }
    }
}
