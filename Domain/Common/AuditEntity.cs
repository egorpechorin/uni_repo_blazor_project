﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Common
{
    public class AuditEntity
    {
        public string CreatedBy { get; set; }
        public DateTime? CreatedDateTime { get; set; }
}
}
