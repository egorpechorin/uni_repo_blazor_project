﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace InsolyWeb.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public bool ChangeMode = false;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet(string? mode)
        {
            if (mode == "create") 
            {
                ChangeMode = true;
            }
            if (mode == "log")
            {
                ChangeMode = false;
            }
        }

        public void ChangeModeR()
        {
            ChangeMode = !ChangeMode;
        }
    }
}
