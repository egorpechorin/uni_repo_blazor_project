using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using App.Queries;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using App.Common.Interfaces;

namespace InsolyWeb.Pages
{
    public class MainModel : PageModel
    {
        public List<PostVM> Posts;
        private readonly ILogger<IndexModel> _logger;
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        private readonly ICurrentUserService _currentUserService;


        public MainModel(ILogger<IndexModel> logger, IMediator mediator, ICurrentUserService currentUserService)
        {
            _logger = logger;
            _mediator = mediator;
            _currentUserService = currentUserService;
        }


        public async void OnGet()
        {
            string userId = _currentUserService.UserId;

            List<PostVM> post = new List<PostVM>();

            post = await _mediator.Send(new GetPostsQuerry() { CurrentUserId = userId });
        }
    }
}
